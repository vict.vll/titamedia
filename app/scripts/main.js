const Http = new XMLHttpRequest();
const clientId = "oHMk_8XBZcvCT-dY49tHJM1BEm7Zw_A6xsq4ftQy9NM";
const url = `https://api.unsplash.com//search/photos/?client_id=${clientId}`;
const options = ["all", "branding", "web", "photography", "app"];

openMenu = () => {
  let menu = document.getElementById("principalMenu");
  if (menu.classList[1] === "close") {
    menu.className = "menu open";
  } else if (menu.classList[1] === "open") {
    menu.className = "menu close";
  }
};

statusSearch = (status) => {
  let inputSearch = document.getElementById("inputSearch");
  let btnOpenInput = document.getElementById("openSearch");
  let btnCloseInput = document.getElementById("closeSearch");
  if (status) {
    btnOpenInput.className = "icon-search hidden";
    btnCloseInput.className = "icon-search";
    inputSearch.className = "open";
  } else {
    inputSearch.value = "";
    btnOpenInput.className = "icon-search";
    btnCloseInput.className = "icon-search hidden";
    inputSearch.className = "close";
  }
};

removeOlders = () => {
  let olderContainer = document.getElementById("containerImages");
  while (olderContainer.firstChild) {
    olderContainer.removeChild(olderContainer.firstChild);
  }
};

getByCategory = (category, reset) => {
  if (reset) {
    removeOlders();
  }
  Http.open("GET", url + `&query=${options[category]}`);
  Http.send();
  Http.onreadystatechange = (e) => {
    removeOlders();
    let imageArray = JSON.parse(Http.responseText);
    addToGallery(imageArray.results);
  };
  activeTab(category);
};

getByQuery = () => {
  let inputValue = document.getElementById("inputSearch");
  let query = inputValue.value;
  if (query === "") {
    query = "all";
  }
  removeOlders();
  Http.open("GET", url + `&query=${query}`);
  Http.send();
  Http.onreadystatechange = (e) => {
    let imageArray = JSON.parse(Http.responseText);
    addToGallery(imageArray.results);
  };
};

disableTabs = () => {
  let tabs = document.getElementsByClassName("option");
  for (let i = 0; i < tabs.length; i++) {
    document.getElementsByClassName("option")[i].className = "option";
  }
};

activeTab = (tab) => {
  disableTabs();
  let activeTab1 = document.getElementsByClassName("option")[tab];
  let activeTab2 = document.getElementsByClassName("option")[tab + 5];
  activeTab1.className = "option active";
  activeTab2.className = "option active";
};

addToGallery = (images) => {
  images.map((element, index) => {
    let div = document.createElement("div");
    div.className = "image";
    div.innerHTML = `<div class="bg-image" style="background-image: url('${element.urls.regular}');"></div><div class="content"><p>${element.alt_description}</p></div>`;
    document.getElementById("containerImages").appendChild(div);
  });
};

changeOrder = (order) => {
  let containerImages = document.getElementsByClassName("container-images")[0];
  if (order === 1) {
    containerImages.className = "container-images mosaic";
  } else if (order === 2) {
    containerImages.className = "container-images";
  }
};

getByCategory(0, false);
